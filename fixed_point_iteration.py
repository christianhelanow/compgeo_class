"""
This module demonstrates the basics of fixed point iteration.

"""
import numpy as np
import pylab as plt

# FIND ROOT OF e^x - 2x - 1 = 0

# start by plotting the equation to get a good intuition of where the
# root(s?) are located
x = np.arange(-5, 5, 0.1)

def root_function(x):
    """Returns e^x-2x-1

    Parameters
    ----------
    x :: float
    """
    return np.e**x - 2*x - 1

# plot the function. the module namespace in python is accesed by plt
plt.ion()                       # turns on the interactive plotting
plt.plot(x, root_function(x), 'b', label="$e^x - 2x - 1$")
plt.plot(x, np.zeros(x.size), 'k--')
plt.legend()
plt.title("Root function problem")

# FIXED POINT PROBLEM
# f(x) = 0 iff x = g(x)
# g(x) gives us a method to iterate a values x_{k+1} = g(x), and
# 'hope' that it converges.
# What is g(x) in this case?
# g(x) = ln(2x+1) ==> g(x) - x = 0
g = lambda x: np.log(2*x+1)

# plot g(x) and x
plt.figure()                 # creates a new figure
plt.plot(x, x, 'r--', label='$x$')
plt.plot(x, g(x), 'b--', label='$g(x) = ln(2x+1)$')
plt.legend()
plt.title("Root function restated as a fixed point problem")

# FIXED POINT ITERATION

def fixed_point_recursive(x, g_function, num_iter=10, tol=1e-4,
                         first_iteration=True):
    """Finds a root of the function f(x) = g(x) - x = 0
 
    Parameters
    ----------
    x : float
        Initial guess of the root.
    g_function : 
        The fixed point function
    num_iter : int, optional
        How many fixed point iterations to go.
    tol : float, optional
        Relative tolerance criterion for when to stop iterations, i.e
        if |x_{k+1} - x_{k}|/x_{k} < tol, the method is deemed to have
        converged
    first_iteration :
        True if first call to functin, False for all recursive calls.
        Default is True.
    Returns
    -------
    guesses_array : array
        The guesses of the root, if found. Otherwise, Nan.
    rel_change : float
        The relative_error at convergence.
    """
    # if the first time the function is called, create a local
    # variable to which we can append the guesses
    if first_iteration:
        # creates a attribute (variable bound to the function) that
        # therefore can be accessed by all recursive function calls
        fixed_point_recursive.guesses_list = [x]
    # get next guess
    x_k = g_function(x)
    # decrease the number of iteration
    num_iter -= 1
    fixed_point_recursive.guesses_list.append(x_k)
    # check tolerance
    rel_change = np.abs(x_k - x)/np.abs(x)
    if rel_change <= tol or num_iter == 0:
        guesses_array = np.array(fixed_point_recursive.guesses_list)
        if num_iter == 0:
            print("The fixed point iteration reached its max iterations!")
        else:
            print("The fixed point iterations converged in {} iterations!".format(
                guesses_array.size))
        return guesses_array, rel_change
    else:
        fixed_point_recursive.guesses_list, rel_change = fixed_point_recursive(
            x_k, g_function, num_iter, tol=tol, first_iteration=False)
        return fixed_point_recursive.guesses_list, rel_change
    
def fixed_point_loop(init_guess, g_function, num_iter=10, tol=1e-4):
    """Finds a root of the function f(x) = g(x) - x = 0
    
    Parameters
    ----------
    init_guess : float
        The initial guess of the root.
    g_function : 
        The fixed point function
    num_iterations : int, optional
        The number of fixed point iterations to take (max).
    tol : float, optional
        Relative tolerance criterion for when to stop iterations, i.e
        if |x_{k+1} - x_{k}|/x_{k} < tol, the method is deemed to have
        converged

    Returns
    -------
    guesses_list : array
        The guesses of the root, if found. Otherwise, Nan.
    rel_error : float
        The relative_error at convergence.
    """
    guesses_list = [init_guess]
    for i in range(num_iter):
        guesses_list.append(g(guesses_list[-1]))
        rel_error = (np.abs(guesses_list[-1] - guesses_list[-2]) /
                     np.abs(guesses_list[-2]))
        if rel_error <= tol:
            return guesses_list, rel_error
    print("The fixed point iteration reached its max iterations!")
    return guesses_list, rel_error

# play with some value
init_guess = 2
root_rec, err_rec = fixed_point_recursive(init_guess, g, num_iter=100)
# we want to plot how the guesses change, from a geometrical point of
# view. This means that some values have to be doubled to plot as a
# line...
x_plot = [i for j in range(len(root_rec)-1) for i in [root_rec[j]]*2]
y_plot = [i for j in range(len(root_rec)-1) for i in root_rec[j:j+2]]
plt.figure()                 # creates a new figure
plt.plot(x, x, 'r-', label='$x$')
plt.plot(x, g(x), 'b-', label='$g(x) = ln(2x+1)$')
plt.plot(x_plot, y_plot, 'k--', label="Iterations", lw=0.5)
plt.plot(x_plot[0], y_plot[0], 'cp', ms=10)
plt.legend()
plt.grid()
plt.title("Fixed point iterations")

