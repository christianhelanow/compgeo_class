"""Recursive function of fibonacci seq.
"""
import numpy as np

def fibonacci_rec(n):
    if n <= 1:
        return n
    else:
        return fibonacci_rec(n-1) + fibonacci_rec(n-2)


def fibonacci_loop(n):
    a, b = 0, 1
    while n > 1:
        a, b = b, b+a
        n -= 1
    return b
